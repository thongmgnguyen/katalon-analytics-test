<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Open Last Row Delete API Key Dialog</name>
   <tag></tag>
   <elementGuidId>70fffac2-37b8-415e-b522-732b7157fb38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-profile']/div[2]/div/form/div[2]/div[2]/div[2]/div/div/table/tbody/tr[19]/td[5]/div/button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@id=&quot;page-profile&quot;]//form[@data-trackid=&quot;api-key&quot;]//table/tbody/tr[last()]/td[last()]//button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id=&quot;page-profile&quot;]//form[@data-trackid=&quot;api-key&quot;]//table/tbody/tr[last()]/td[last()]//button[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>(//button[@type='button'])[40]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='page-profile']/div[2]/div/form/div[2]/div[2]/div[2]/div/div/table/tbody/tr[19]/td[5]/div/button</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='a4459861-0b3d-44c7-a0d3-db98754a37f3'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='asdasdasdwefdsadsgfasdfsd'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon Analytics'])[1]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Beta'])[1]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//tr[19]/td[5]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
