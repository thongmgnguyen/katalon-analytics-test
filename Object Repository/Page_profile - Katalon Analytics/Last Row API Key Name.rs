<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Last Row API Key Name</name>
   <tag></tag>
   <elementGuidId>a7952444-7f4e-4f1d-9960-c32fce527ed4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-profile']/div[2]/div/form/div[2]/div[2]/div[2]/div/div/table/tbody/tr[last()]/td[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@id=&quot;page-profile&quot;]//form[@data-trackid=&quot;api-key&quot;]//table/tbody/tr[last()]/td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id=&quot;page-profile&quot;]//form[@data-trackid=&quot;api-key&quot;]//table/tbody/tr[last()]/td[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='page-profile']/div[2]/div/form/div[2]/div[2]/div[2]/div/div/table/tbody/tr/td</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::td[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Created At'])[1]/following::td[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon Analytics'])[1]/preceding::td[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//td</value>
   </webElementXpaths>
</WebElementEntity>
