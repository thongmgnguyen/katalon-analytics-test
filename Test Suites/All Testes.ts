<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>All Testes</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5a9c9647-07a4-4b3b-a9d4-8fbc55f15495</testSuiteGuid>
   <testCaseLink>
      <guid>d7114a8b-43c5-4e8f-956d-92c1f1cf40da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile Page/Create API Key</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>07dfb35c-4ddf-498e-a35c-6fff381a9633</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>149db8e2-d489-4d48-8b3e-27f1be834f99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile Page/Remove API Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99e19b66-3426-4279-bdce-e82972d70efa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Projects Page/Create Project</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cd98ab65-ca66-4e62-9d78-093d8acb925c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b1efd576-24df-4889-ac97-182126f69a03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Projects Page/Delete project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e146eae7-9b08-4994-8600-cdf4d59048a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Projects Page/Edit Project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10c6cf9f-ab8e-467e-8b82-099974bc4fba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Teams Page/Edit Team</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b42604bb-5658-422c-b8bd-3d4810eb7de5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Teams Page/Navigate to Projects Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15b5506f-f9e9-4360-891a-a42ff2a769db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Teams Page/Navigate to Users Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dd7dd58-b67b-4830-b0b4-e92e89bf22aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Page/Cancel delete invited user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2f89558-272c-4f73-94ee-fe137770ee93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Page/Delete invited user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e87d5be-1c57-458b-b87c-d91f7c0d6f95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Page/Invite invited user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc398d06-ddbc-4b84-a791-e19a655edb3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Page/Invite member of team</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60f526d1-4b40-4433-b701-40f2af60a0a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Page/Invite new user</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
