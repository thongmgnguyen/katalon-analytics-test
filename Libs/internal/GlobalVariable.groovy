package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object timeout
     
    /**
     * <p></p>
     */
    public static Object delay
     
    /**
     * <p></p>
     */
    public static Object host
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['username' : 'quile@kms-technology.com', 'password' : 'RigbBhfdqOBGNlJIWM1ClA==', 'timeout' : 5, 'delay' : 3, 'host' : 'https://localhost:8444/'])
        allVariables.put('production', allVariables['default'] + ['username' : 'quile@kms-technology.com', 'password' : 'RigbBhfdqOBGNlJIWM1ClA==', 'timeout' : 5, 'delay' : 3, 'host' : 'https://analytics.katalon.com/'])
        allVariables.put('staging', allVariables['default'] + ['username' : 'quile@kms-technology.com', 'password' : 'RigbBhfdqOBGNlJIWM1ClA==', 'timeout' : 5, 'delay' : 3, 'host' : 'https://analytics-staging.katalon.com/'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        def selectedVariables = allVariables[profileName]
		
		for(object in selectedVariables){
			String overridingGlobalVariable = RunConfiguration.getOverridingGlobalVariable(object.key)
			if(overridingGlobalVariable != null){
				selectedVariables.put(object.key, overridingGlobalVariable)
			}
		}

        username = selectedVariables["username"]
        password = selectedVariables["password"]
        timeout = selectedVariables["timeout"]
        delay = selectedVariables["delay"]
        host = selectedVariables["host"]
        
    }
}
