import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/All Testes')

suiteProperties.put('name', 'All Testes')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("/Users/quile/Documents/Projects/katalon-analytics-ui-test/Reports/All Testes/20190118_145551/execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/All Testes', suiteProperties, [new TestCaseBinding('Test Cases/Profile Page/Create API Key', 'Test Cases/Profile Page/Create API Key',  null), new TestCaseBinding('Test Cases/Profile Page/Remove API Key', 'Test Cases/Profile Page/Remove API Key',  null), new TestCaseBinding('Test Cases/Projects Page/Create Project', 'Test Cases/Projects Page/Create Project',  null), new TestCaseBinding('Test Cases/Projects Page/Delete project', 'Test Cases/Projects Page/Delete project',  null), new TestCaseBinding('Test Cases/Projects Page/Edit Project', 'Test Cases/Projects Page/Edit Project',  null), new TestCaseBinding('Test Cases/Teams Page/Edit Team', 'Test Cases/Teams Page/Edit Team',  null), new TestCaseBinding('Test Cases/Teams Page/Navigate to Projects Page', 'Test Cases/Teams Page/Navigate to Projects Page',  null), new TestCaseBinding('Test Cases/Teams Page/Navigate to Users Page', 'Test Cases/Teams Page/Navigate to Users Page',  null), new TestCaseBinding('Test Cases/Users Page/Cancel delete invited user', 'Test Cases/Users Page/Cancel delete invited user',  null), new TestCaseBinding('Test Cases/Users Page/Delete invited user', 'Test Cases/Users Page/Delete invited user',  null), new TestCaseBinding('Test Cases/Users Page/Invite invited user', 'Test Cases/Users Page/Invite invited user',  null), new TestCaseBinding('Test Cases/Users Page/Invite member of team', 'Test Cases/Users Page/Invite member of team',  null), new TestCaseBinding('Test Cases/Users Page/Invite new user', 'Test Cases/Users Page/Invite new user',  null)])
