import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if ("".equals(apiKeyName)) {
	apiKeyName = UUID.randomUUID().toString()
}

WebUI.callTestCase(findTestCase('Profile Page/The profile page is loaded successfully'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Profile - Katalon Analytics/Button Open Create API Key Dialog'))

WebUI.setText(findTestObject('Page_Profile - Katalon Analytics/Input API Key Name'), apiKeyName)

WebUI.click(findTestObject('Page_Profile - Katalon Analytics/Button Create'))

WebUI.verifyElementPresent(findTestObject('Toast Success'), GlobalVariable.timeout)

WebUI.delay(GlobalVariable.delay)

String firstApiKeyName = WebUI.getText(findTestObject('Page_Profile - Katalon Analytics/Last Row API Key Name'))

WebUI.verifyEqual(apiKeyName, firstApiKeyName)
