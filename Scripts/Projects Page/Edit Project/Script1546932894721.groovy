import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String projectName = UUID.randomUUID().toString()

String newProjectName = UUID.randomUUID().toString()

WebUI.callTestCase(findTestCase('Projects Page/Create Project'), [('projectName') : projectName], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(GlobalVariable.delay)

String projectId = WebUI.getAttribute(findTestObject('Object Repository/Page_Administration  Projects - Kat/Project Table First Row Project Id'), 
    'id', FailureHandling.STOP_ON_FAILURE)

projectId = projectId.substring('project-id-'.length())

WebUI.click(findTestObject('Page_Administration  Projects - Kat/Button Open Edit Project Dialog', [('projectId') : projectId]))

WebUI.setText(findTestObject('Object Repository/Page_Administration  Projects - Kat/Input Edit Project Name'), newProjectName)

WebUI.click(findTestObject('Object Repository/Page_Administration  Projects - Kat/Button Edit Dialog Save'))

WebUI.verifyElementPresent(findTestObject('Toast Success'), GlobalVariable.timeout)

