import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Teams Page/The teams page is loaded successfully'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(GlobalVariable.delay)

String teamId = WebUI.getAttribute(findTestObject('Page_Administration  Teams - Katalo/Team Table First Row Team Id'),
	'id', FailureHandling.STOP_ON_FAILURE)

teamId = teamId.substring('team-id-'.length())

WebUI.click(findTestObject('Page_Administration  Teams - Katalo/Button Manage Users', [('teamId') : teamId]))

WebUI.delay(GlobalVariable.delay)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Administration  Users - Katalo/Users Breadcrumb'), GlobalVariable.timeout)

