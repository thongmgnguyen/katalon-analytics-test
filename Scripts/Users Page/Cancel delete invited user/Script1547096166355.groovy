import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Test Cases/Users Page/Invite new user'), [:], FailureHandling.STOP_ON_FAILURE)

String email = WebUI.getText(findTestObject("Object Repository/Page_Administration  Users - Katalo/Invited User Table First Row Email"), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Administration  Users - Katalo/Button Open Remove First Row Invited User Dialog'));

WebUI.click(findTestObject('Object Repository/Page_Administration  Users - Katalo/Button Cancel'));

WebUI.delay(GlobalVariable.delay, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent(email, false)

